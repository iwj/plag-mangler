# plag-mangler - planar graph handling utility and layout optimiser
#
#  Copyright (C) 2019 Ian Jackson
#
#  This program is free software: you can redistribute it and/or modify
#  it under the terms of the GNU Affero General Public License as published by
#  the Free Software Foundation, either version 3 of the License, or
#  (at your option) any later version.

#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU Affero General Public License for more details.
#
#  You should have received a copy of the GNU Affero General Public License
#  along with this program.  If not, see <http://www.gnu.org/licenses/>.

i=mv -f $@.tmp $@
o= >$@.tmp && $i

CXXFLAGS += -Wall

CARGO ?= cargo
RST2HTML ?= rst2html

export RUSTFLAGS += -Lboost -lstdc++

PROGRAM=target/debug/plag-mangler

HTMLDOCS= $(addsuffix .html, README plag)

all:	$(PROGRAM)

full:	all check release-cargo docs docs-dir

boost/glue.o: boost/glue.cpp
boost/libglue.a: boost/glue.o
	rm -rf target # rebuilds too much but cargo is awful
	rm -f $@; $(AR) cqv $@ $^

CARGO_DEPS= boost/libglue.a phony

$(PROGRAM): $(CARGO_DEPS)
	+$(CARGO) build $(CARGO_FLAGS)

.PHONY: phony

release-cargo: $(CARGO_DEPS)
	+$(CARGO) test $(CARGO_FLAGS) --release
	+$(CARGO) build $(CARGO_FLAGS) --release

check-cargo: $(PROGRAM) $(CARGO_DEPS)
	+$(CARGO) test $(CARGO_FLAGS) -- $${RUST_TEST_VERBOSE:+--nocapture} $${RUST_TEST_NAME:+--test} $${RUST_TEST_NAME}

docs: $(HTMLDOCS)

docs-dir: $(HTMLDOCS) Makefile
	rm -rf $@ $@.tmp; mkdir $@.tmp
	ln -sf README.html $@.tmp/index.html
	cp $(filter-out Makefile, $^) $@.tmp/. && $i

%.html: %.rst
	$(RST2HTML) $< $o

%-checks: $(PROGRAM)
	$(MAKE) -C checks $*

check: check-cargo check-checks

www=ianmdlvl@chiark:public-html/plag-mangler

docs-to-www: docs-dir
	rsync -aH --delete docs-dir/. $(www)/.


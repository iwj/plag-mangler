// plag-mangler - planar graph handling utility and layout optimiser
//
// chrobakpayne.rs - Rust side of the glue to the Boost graph library.
//
//   Copyright (C) 2019 Ian Jackson
//
//  plag-mangler is free software: you can redistribute it and/or modify
//  it under the terms of the GNU Affero General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU Affero General Public License for more details.
//
//  You should have received a copy of the GNU Affero General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.


use super::*;

//use std::io;
//use std::io::Write;

extern crate arrayvec;
extern crate libc;

//extern crate arrayvec;
//use arrayvec::ArrayVec;

use libc::size_t;

#[link(name="glue")]
extern "C" {
    pub fn cxx_chrobak_payne(
        nvertices : size_t,
        edge_data : *const [size_t; 2],
        n_edge_data : size_t,
	embed_data : *const Cxx_Embed_V_Data_Ent,
	n_embed_data : size_t,
	order : *const size_t,
	coord_result : *mut Cxx_Coord,
    ) -> libc::c_int;
}

#[repr(C)]
pub struct Cxx_Embed_V_Data_Ent {
    n : size_t,
    l : *const size_t,
}

#[repr(C)]
#[derive(Copy,Clone)]
pub struct Cxx_Coord {
    x : libc::ssize_t,
    y : libc::ssize_t,
}

impl PlanarGraph {
    pub fn chrobak_payne(&mut self, pco : &[usize]) {
        let g = self;
        g.ensure_invxs();
        let nvertices = g.vs.len();
        let (count, mut embed) = g.edge_xref_storage::<size_t>(!0);
        let mut edges = Vec::with_capacity(count);

        for (_vi, v) in g.vs.iter().enumerate() {
            for e in &v.adj {
                let e = EndRef(e);
                if e.selector() == EndSelector::B { continue }

                let edge_num = edges.len();
                edges.push([ e.source() as size_t,
                                 e.target() as size_t ]);

                for &end in &ENDS {
                    embed[
                        e .srcs[end]
                    ][
                        e.m.borrow().invx[end]
                    ] = edge_num as size_t;
                }
            }
        }
        let embed : Vec<Cxx_Embed_V_Data_Ent> =
            embed
            .iter()
            .map(|v| {
                assert!(v.iter().all(|e| *e < edges.len()));
                Cxx_Embed_V_Data_Ent {
                    n : v.len(),
                    l : v.as_ptr(),
                }
            })
            .collect();

        assert!(pco.len() == nvertices);
        let mut coords : Vec<Cxx_Coord> =
            vec![ Cxx_Coord{ x:0, y:0 } ; nvertices ];

        let ok = unsafe {
            cxx_chrobak_payne(nvertices,
                            edges.as_ptr(), edges.len(),
                            embed.as_ptr(), embed.len(),
                            pco.as_ptr(),
                            coords.as_mut_ptr())
        };
        assert_eq!(ok,1);

        for (vi,v) in g.vs.iter_mut().enumerate() {
            let c = coords[vi];
            // Boost's Chrobak-Payne likes to do things clockwise
            v.coordinates = Some([ -c.x as Coordinate,
                                    c.y as Coordinate ]);
        }
    }
}

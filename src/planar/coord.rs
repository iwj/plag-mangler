// plag-mangler - planar graph handling utility and layout optimiser
//
// cooord.rs - coordinate type, for convenience
//
//   Copyright (C) 2019 Ian Jackson
//
//  plag-mangler is free software: you can redistribute it and/or modify
//  it under the terms of the GNU Affero General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU Affero General Public License for more details.
//
//  You should have received a copy of the GNU Affero General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.


use std::ops;

#[derive(Copy,Clone,Debug,PartialEq)]
pub struct Point(pub f64, pub f64);

impl Point {
    pub fn new() -> Self { Point(0.,0.) }

    pub fn atan2(&self) -> f64 { self.1.atan2(self.0) }
    pub fn len2(&self) -> f64 { self.0 * self.0 +
                                self.1 * self.1 }
    pub fn len(&self) -> f64 { self.len2().sqrt() }
}

impl ops::Sub for Point {
    type Output = Self;
    fn sub(self, rhs : Self) -> Self { Point( self.0 - rhs.0,
                                              self.1 - rhs.1 ) }
}

impl Default for Point {
    fn default() -> Self { Point::new() }
}

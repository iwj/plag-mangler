// plag-mangler - planar graph handling utility and layout optimiser
//
// pco.rs - planar canonical order
//
//   Copyright (C) 2019 Ian Jackson
//
//  plag-mangler is free software: you can redistribute it and/or modify
//  it under the terms of the GNU Affero General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU Affero General Public License for more details.
//
//  You should have received a copy of the GNU Affero General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.


use super::*;

struct PcoState<'p> {
    g : &'p mut PlanarGraph,
    vs : Vec<PcoVertex>,
    ready : VecDeque<VertexRef>,
    output : VecDeque<VertexRef>,
}

#[derive(Debug)]
struct PcoVertex {
    chordish : Option<isize>, // None for non-outside vertexes
    in_ready : bool,
}

impl PcoVertex {
    fn ready_p(&self) -> bool { self.chordish == Some(2) }
}

impl<'p> PcoState<'p> {
    fn chordish_adjust(&mut self, vi : VertexRef, adj : isize) {
        let pco = self;
        let v = &mut pco.vs[vi];
        *v.chordish.as_mut().unwrap() += adj;
//eprintln!("CHORDISH_ADJUST {} {} {:?}", &v!(pco.g,vi), adj, v.chordish);
        if v.ready_p() && !v.in_ready {
            v.in_ready = true;
            pco.ready.push_back(vi);
        }
        // We don't remove things here.  We defer that to our loop.
        // That avoids having another doubly linked list.
        // This doesn't change the algorithmic complexity.
    }

    fn add_outside(&mut self, vi : VertexRef) {
        let pco = self;
//eprintln!("ADD_OUTSIDE {}", &v!(pco.g,vi));
        assert!(pco.vs[vi].chordish.is_none());
        pco.vs[vi].chordish = Some(0);
        for e in &v!(pco.g,vi).adj {
            let e = EndRef::new(&e);
            let ui = e.target();
//eprintln!("ADD_OUTSIDE {}", e.debug(&pco.g));
//eprintln!("          {} {} {}", e.source(), e.target(), e.selector());
            if pco.vs[ui].chordish.is_some() {
                pco.chordish_adjust(vi, 1);
                pco.chordish_adjust(ui, 1);
            }
        }
    }
}

impl PlanarGraph {
    pub fn planar_canonical_order(&self, outside : [VertexRef;3])
                                  -> Result<Vec<VertexRef>>
    {
        // How to Draw a Planar Graph on a Grid
        // De Fraysseix, Pach & Pollack
        // Combinatorica 10(1)(1990) 41-51
        // ^ p4, proof that a canonical ordering exists, etc.
        //
        // A linear-time algorithm for drawing a planar graph on a grid
        // Chrobak & Payne
        // Information Processing Letters 54(1995) 241-246
        // ^ p4, col 1, s3, 2nd para, "Also, the exist": O(N) algorithm

        // algorithm
        //  designate outside face as vertices u0 u1 w_n
        //  we strip off one vertex w_k at a time, generating graph G_k-1
        //  C_k is cycle of edges of outside face of G_k

        // definitions (for some k)
        //   "outside vertex": for some k, a vertex of C_k
        //   chordish edge - edge both of whose vertices are outside
        //    (either a chord or an outside edge ie member of C_k)

        // we maintain
        //   for each vertex (pco.vs)
        //      is it on the current outside face ?
        //      if it is on the current outside face,
        //         how many chordish edges does it have
        //      bodge: add 2 spuriously to u0 and u1 chordish edge count
        //
        //   list of vertices on the outside face
        //     with only 2 chordish edges
        //     (pco.ready, pco.vs.in_ready; see PcoState::chordish_set)
        

        let mut g = self.clone();
        let u = &outside[0..2];

        // initialisation
        //  visit each outside vertex and set its outsideness
        //  visit each outside vertex and count chordish edges

        let n = self.vs.len();
        if n < 3 { Err(GraphTooSmall)? }
        let mut pco = PcoState {
            g : &mut g,
            vs : Vec::with_capacity(n),
            ready : VecDeque::with_capacity(n-2),
            output : VecDeque::with_capacity(n),
        };

//eprintln!("SETUP 1");
        for _vi in 0..n {
            pco.vs.push( PcoVertex {
                chordish : None,
                in_ready : false,
            } );
        }
//eprintln!("SETUP 2");
        for &vi in u {
            pco.add_outside(vi);
            pco.chordish_adjust(vi, 2);
            // ^ prevents us selecting u0 and u1 for removal
        }
//eprintln!("SETUP 3");
        pco.add_outside(outside[2]);

        // loop
        //  starting with G_k etc.
        //  pop a vertex from the todo list
        //  it must have 2 chordish edges
        //  it is going to be removed as w_k, giving G_k-1
        //    so add it to the planar ordering (this happens in reverse order)
        //
        //  iterate over its edges
        //    other-end vertices that were not outside become outside
        //      compute their chordish edge count
        //        and while there adjust
        //        their neighbours' chordish edge counts,
        //    other-end vertices that were outside
        //      lose a chordish edge, so adjust chordish edge counts
        //
        //   "adjust chordish edge counts" involves adding/removing vertex
        //     to/from list of outside vertices

        loop {
            let vi = pco.ready.pop_front();
            if vi.is_none() { break }
            let vi = vi.unwrap();
//eprintln!("LOOP {} ?", &v!(pco.g, vi));
            {
                let v = &mut pco.vs[vi];
                assert!(v.in_ready);
                v.in_ready = false;
                if !v.ready_p() {
                    // should have been removed when chordish increased
                    continue
                }
            }
            // we are removing vi
            pco.output.push_front(vi);            
            for e in &v!(pco.g,vi).adj {
                let e = EndRef(e);
//eprintln!("LOOP {}", e.debug(&pco.g));
                let f = e.reverse();
                let ui = f.source();
                // consider its neighbour ui
                v!(pco.g,ui).adj.remove(&f);
                match pco.vs[ui].chordish {
                    None => {
                        // it was inside; now it is outside
                        pco.add_outside(ui);
                    },
                    Some(_) => {
                        // it was outside; it has one fewer chordish edge
                        pco.chordish_adjust(ui, -1);
                    },
                }
            }
        }
        if pco.output.len() != n-2 { Err(PCOIncomplete)?; }
        for &ui in u.iter().rev() {
            if pco.vs[ui].chordish != Some(3) { Err(PCOBaseNotReady)?; }
            pco.output.push_front(ui);
        }
        Ok(pco.output.into())
    }
}

// plag-mangler - planar graph handling utility and layout optimiser
//
// outer.rs - tracking and manipulating outer face(s) etc.
//
//   Copyright (C) 2019 Ian Jackson
//
//  plag-mangler is free software: you can redistribute it and/or modify
//  it under the terms of the GNU Affero General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU Affero General Public License for more details.
//
//  You should have received a copy of the GNU Affero General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.


use super::*;

const DEBUG_FLAG_HERE : DebugFlags = DEBUG_OUTER;

impl PlanarGraph {
    
    /// sets each vertex to be outer iff it has
    /// an incident edge marking an incident face as outer
    /// (or if `all`, if *all* incident edges so mark)
    pub fn outer_face2vertex(&mut self, all : bool) {
        let g = self;

        for v in &mut g.vs {
            v.outer = all ^ v.adj
                .iter()
                .map(|e| EndRef(e))
                .any(|e| all ^ *em![e,.faceouter]);
        }
    }
    /// sets each face side to be outer iff all incident
    /// vertices on that face are outer
    pub fn outer_vertex2face(&mut self) ->Result<()> {
        let g = self;

        g.set_all_invxs(0); // means `face done'

        for v in &g.vs {
            for face in &v.adj {
                let face = EndRef(face);
                if *em![face,.invx] != 0 { continue }

                let mut all_outer = true;
                for_face!{
                    e, (g,face),
                    all_outer &= v!(g,e,source).outer;
                }
                for_face!{
                    e, (g,face),
                    *em_mut![e,.invx] = 1;
                    *em_mut![e,.faceouter] = all_outer;
                }
            }
        }
        Ok(())
    }

    /// finds any one face with any one edge side marked outer
    /// expects that all of its edge sides are marked outer
    /// and returns the indices of the face's vertices
    pub fn find_outer(&self) -> Result< Vec<VertexRef> > {
        let g = self;

        for v in &g.vs {
            for face in &v.adj {
                let face = EndRef(face);
                if !*em![face,.faceouter] { continue }

                let mut faceouter = Vec::new();
                for_face!{
                    e, (g,face),
                    if !*em![e,.faceouter] { Err(OuterInconsistent)? }
                    faceouter.push(e.source());
                }
                return Ok(faceouter);
            }
        }

        Err(OuterUnspecified)?;
        panic!();
    }

    /// Merges any outer faces which touch at an edge, without
    /// changing any vertex order other than of vertices with no
    /// non-outer incident face (`strictly outer vertices').
    ///
    /// If the outer faces are contiguous, the result will have only
    /// one outer face (obviously).  The approach is such that if the
    /// input was connected and there was only one strictly outer
    /// vertex, the result will still be connected.
    ///
    /// Algorithm:
    ///
    /// First we mark vertices as outer iff they are strictly outer.
    ///
    /// Then we split every strictly outer vertex into one vertex for
    /// each incident edge.  This joins all the incident outerfaces.
    ///
    /// Finally for any edge both sides of which are outer, but
    /// where neither end is strictly outer, splits the edge into
    /// two edges, each ending in a new singleton vertex, with a gap
    /// in between.  This joins the two outerfaces without changing
    /// the vertex order, or outer face incidence, at either vertex.

    pub fn split_outer(&mut self, namesep : char) -> Result<()> {
        let g = self;

        g.outer_face2vertex(true);

        for vi in 0..g.vs.len() {
            if !v!(g,vi).outer { continue }

            debugln!(g,"SPLIT {}",v!(g,vi));
            // split v into one per edge
            let mut ei = 0;
            let mut e = v!(g,vi).adj.iter();
            'edge_loop: while Option::is_some(&e) {
                let ne = list::ListIterator::from_cursor(
                    e.as_ref().unwrap().get_next_prev(false)
                );
                'edge_loop_body: loop {
                    let e = EndRef(e.clone().unwrap());
                    debugln!(g,"SPLIT {} {}",v!(g,vi),e.debug(g));
                    let suffix = format!("{}o",ei);
                    v!(g,vi).adj.remove(&e);

                    let reuse_vi = !ne.is_some();
                    let nvi = if reuse_vi { vi } else { g.vs.len() };
                    let nname = intern_uniquely(
                        &mut g.intern, namesep, &suffix,
                        nvi, String::clone( &v!(g,e.target()).name ),
                    );

                    let mut nvd = VertexData {
                        adj : Default::default(),
                        name : nname,
                        outer : true,
                        coordinates : None, // avoid piling them all up
                    };

                    let m = e.m.borrow();

                    let mut ne = Edge {
                        ll : Default::default(),
                        srcs : e.srcs,
                        m : RefCell::new( EdgeMut {
                            added : m.added,
                            invx : INVXS_INVALID,
                            faceouter : PerEnd([true;2]),
                            facename : m.facename.clone(), // multiple names
                        }),
                    };
                    ne.srcs[ e.selector() ] = nvi;
                    let ne = Rc::new(ne);
                    let ne = Pointer::with_selector(&ne, e.selector());
                    let ne = EndRef(ne);

                    nvd.adj.push_back(&ne);

                    if reuse_vi {
                        // We have now removed all the edges from vi,
                        // including this final one.
                        g.intern.remove(&v!(g,vi).name);
                        g.vs[vi] = nvd;
                    } else {
                        g.vs.push(nvd);
                    }

                    // Now, we have a new vertex.  The end at what was
                    // e and is now ne has the new (ie, copied) edge
                    // and no other edges.

                    // Sort out the far end: remove the old edge, and
                    // put in the new one in its place.  We must do
                    // this in this order so we keep our place:
                    let f  = e.reverse();
                    let nf = ne.reverse();
                    v!(g,e,target).adj.insert(&nf,&f,false);
                    v!(g,e,target).adj.remove(&f);

                    break 'edge_loop_body; // not a real loop
                }
                e = ne;
                ei += 1;
            }
        }

        for svi in 0..g.vs.len() {
            if v!(g,svi).outer { continue }

            for e in &v!(g,svi).adj {
                let e = EndRef(e);
                if e.selector() == EndSelector::B { continue }
                if v!(g,e,target).outer { continue }

                let f = e.reverse();
                if !(*em![e,.faceouter] && *em![f,.faceouter]) { continue }

                debugln!(g,"CHOP {}",e.debug(g));

                for ref e in &[&e,&f] {
                    debugln!(g,"  CHOP {}",e.debug(g));
                    // e is the anchor vertex
                    // it will become the A end of ne
                    let old_f = e.reverse();

                    let nvi = g.vs.len();
                    let nname = intern_uniquely(
                        &mut g.intern, namesep, "o",
                        nvi, v!(g,e,target).name.to_string(),
                    );
                    let mut nvd = VertexData {
                        adj : Default::default(),
                        name : nname,
                        outer : true,
                        coordinates : None,
                    };
                    let m = e.m.borrow();
                    let ne = Rc::new( Edge {
                        ll : Default::default(),
                        srcs : PerEnd([ e.source(), nvi ]),
                        m : RefCell::new( EdgeMut {
                            added : m.added,
                            invx : INVXS_INVALID,
                            faceouter : PerEnd([true;2]),
                            facename : PerEnd([
                                em![e    ,.facename].clone(),
                                em![old_f,.facename].clone(),
                            ]),
                        }),
                    });
                    let ne = Pointer::with_selector(&ne, EndSelector::A);
                    let ne = EndRef(ne);
                    let new_f = ne.reverse();

                    nvd.adj.push_back(&new_f);
                    v!(g,e,source).adj.insert(&ne,&e,false);

                    g.vs.push(nvd);
                }
                for ref e in &[&e,&f] {
                    v!(g,e,source).adj.remove(&e);
                }
            }
        }

        Ok(())
    }

}

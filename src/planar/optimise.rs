// plag-mangler - planar graph handling utility and layout optimiser
//
// optimise.rs - layout optimisation including glue to NLopt and GSL
//
//   Copyright (C) 2019 Ian Jackson
//
//  plag-mangler is free software: you can redistribute it and/or modify
//  it under the terms of the GNU Affero General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU Affero General Public License for more details.
//
//  You should have received a copy of the GNU Affero General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.

/*

 RUST_BACKTRACE=1 target/debug/plag-mangler <examples/outersplit2.plag R OUTER-SPLIT B T OUTER-F2V OUTER-F12VA PCO CP RAE W t.plag WG t.dot && neato t.dot -Tps >t.ps

 RUST_BACKTRACE=1 target/debug/plag-mangler <t.plag R ANNEAL 2>&1  |less

RUST_BACKTRACE=1 target/debug/plag-mangler <t.plag R ANNEAL |qtdebug/vtrace

*/

use super::*;

use std::ops::Deref;
use std::f64::consts::*;
use std::time::{Instant,Duration};

use self::coord::Point;

//----- types -----

const TAU : f64 = PI * 2.;

#[derive(Copy,Clone,Default,Debug)]
struct Tuning {
    face_interior_angle_for_halfcost  :f64,
    face_interior_angle_at0_cost      :f64,
    line_length_at0_cost              :f64,
    line_length_target                :f64,
    line_length_triangle_target       :f64,
    line_length_shrink_strength       :f64,
    vertex_edge_order_cost_factor     :f64,
}

impl Tuning {
    fn new() -> Tuning {
        let mut t : Tuning = Default::default();
        t.face_interior_angle_for_halfcost = 1.0; // [rad]
        t.face_interior_angle_at0_cost = 0.15; // [$]
        t.line_length_at0_cost = 1.0; // [$]
        t.line_length_target = 1.0; // [mm] (or whatever the units are)
        t.line_length_shrink_strength = 1./100.; // [$/$], relative to length_at0
        t.line_length_triangle_target = 1.75; // [mm] (or whatever the units are)
        t.vertex_edge_order_cost_factor = 1000.0; // [$/angle]
        t
    }
}

#[derive(Copy,Clone,Default,Debug)]
struct TuningComputed {
    face_interior_angle_cost_factor  : f64,
    line_length_short_factor         : f64,
    line_length_long_factor          : f64,
    input : Tuning,
}

pub struct Context {
    faces : Vec<Vec<VertexRef>>,
    edges : Vec<Vec<VertexRef>>, // c.edges[vi][] = vw
    t : TuningComputed,
    vnames : Vec<Rc<String>>,
    debug : DebugFlags,
}

#[derive(Clone)]
pub struct Solution {
    c : Rc<Context>,
    xs : Vec<f64>, // each pair is a Point
}

pub struct SolutionRef<'x> {
    xs : &'x [f64],
    c : &'x Context,
}

type Cost = f64;

//----- convenience -----

impl<'x> SolutionRef<'x> {
    fn p(&self, i : usize) -> Point {
        Point(self.xs[i*2], self.xs[i*2+1])
    }
    fn vname(&self, i : usize) -> &Rc<String> {
        &self.c.vnames[i]
    }
}

impl Deref for TuningComputed {
    type Target = Tuning;
    fn deref(&self) -> &Tuning { &self.input }
}

//----- progres printing macros -----

// these work with $g being a Context too
macro_rules! progressp { ($g:expr) => ($g.debug & DEBUG_OPTIM_PROGRESS != 0) }
macro_rules! progress {
    ($g:expr, $($rhs:tt)*) => { (if progressp!($g) {   print!($($rhs)*); }) }
}
macro_rules! progressln {
    ($g:expr, $($rhs:tt)*) => { (if progressp!($g) { println!($($rhs)*); }) }
}

macro_rules! for_face_corners {
    { ($s:expr, $dbg:expr;
       $edge_vec:ident, $gamma:ident, $vi:ident, $last_vi:ident,
       $last2_vi:ident, $face:ident) {
        $($body:tt)*
    } } => {
        for face in &$s.c.faces {
            let mut last2_vi = !0;
            let mut last_vi = !0;
            let mut last_v = Point::new();
            let mut last_theta = 0.;
            let $face = face;
            debugln!($s.c, "{}F {}-{}", $dbg,
                     $s.vname(face[0]), $s.vname(face[1]));

            for (&vi,prep) in
                face[ face.len()-2 .. ].iter() .zip(iter::repeat(true))
                .chain( face.iter() .zip(iter::repeat(false)) )
            {
                let v = $s.p(vi);
                let edge_vec = v - last_v;
                let theta = edge_vec.atan2();
                let gamma = TAU/2. - (theta - last_theta); // interior angle

                let $gamma = normalise_corner_angle(gamma);
                let $edge_vec = edge_vec;
                let $vi = vi;
                let $last_vi = last_vi;
                let $last2_vi = last2_vi;

                debugln!($s.c, " {}E {}{} v={:?} vec={:?} theta={} gamma={}",
                         $dbg, $s.vname(vi), if prep { "P" } else { "" },
                         v, $edge_vec, theta, $gamma);

                if !prep {
                    $($body)*
                }

                last2_vi = last_vi;
                last_vi = vi;
                last_v = v;
                last_theta = theta;
            }
        }
    }
}

//----- cost function -----

use self::costmod::*;
mod costmod { // this lets us have a nicely scoped DEBUG_FLAG_HERE
use super::*;

const DEBUG_FLAG_HERE : DebugFlags = DEBUG_OPTIM_COST;

struct CostTracker { cost : Cost, debug : DebugFlags, }

impl CostTracker {
    fn new(c : &Context) -> Self {
        CostTracker { cost : 0., debug : c.debug }
    }

    fn val(&self) -> Cost { self.cost }

    fn add(&mut self, why : std::fmt::Arguments, d : Cost) {
        #![allow(path_statements)] why;
        const DEBUG_FLAG_HERE : DebugFlags = DEBUG_OPTIM_COST;
        debugln!(self, " cost+={:8.3} {}", d, why);
        self.cost += d;
    }
}

pub fn normalise_corner_angle(gamma : f64) -> f64 {
    (gamma + TAU) % TAU
}

pub fn line_length_target(c : &Context, face : &[VertexRef]) -> f64 {
    let t = &c.t;
    if face.len() != 3 {
        t.line_length_target
    } else {
        t.line_length_triangle_target
    }
}

pub fn costf(s : SolutionRef) -> f64 {
    let c = &s.c;
    let mut cost = CostTracker::new(c);
    let t = &s.c.t;

    for_face_corners!{ (s, "C";
                        edge_vec, gamma, vi, last_vi, _last2_vi, face) {
        if vi > last_vi {
            // process this edge now
            let dl = edge_vec.len() - line_length_target(c, face);
            cost.add(format_args!("line_length dl={}",dl),
                    dl *
                    if dl < 0. {
                        t.line_length_short_factor
                    } else {
                        dl * t.line_length_long_factor
                    }
            );
        }

        cost.add(format_args!("angle gamma={}", gamma), {
            let gamma =
                if gamma > 0.5*TAU { TAU - gamma } else { gamma };
            t.face_interior_angle_cost_factor
                / (gamma + t.face_interior_angle_for_halfcost)
        });
    }}

    for vi in 0..c.edges.len() {
        let viol = vertex_edge_order_viol(&s, vi);
        if viol > 0. {
            cost.add(format_args!("vertex {} viol", vi),
                     viol
                     * t.vertex_edge_order_cost_factor
            );
        }
    }

    cost.val()
}

pub fn nconstraints(c : &Context) -> usize {
    let nedges : usize = c.edges.iter().map(|es| es.len()).sum();
    c.edges.len() + nedges/2
}
    
pub fn vertex_edge_order_viol(s : &SolutionRef, vi : usize) -> f64
{
    let c = &s.c;

    debugln!(c, "C {}", s.vname(vi));

    let vedges = &s.c.edges[vi];

    let mut total_gamma = 0.;
    let mut last_theta = 0.;
    let mut biggest_gamma : f64 = 0.;
    let mut smallest_gamma : f64 = 0.;

    for (&wi,prep) in
        iter::once( (&vedges[ vedges.len()-1 ], true) )
        .chain( vedges.iter() .zip(iter::repeat(false)) )
    {
        let edge_vec = s.p(wi) - s.p(vi);
        let theta = edge_vec.atan2();
        let gamma = theta - last_theta;
        let gamma = normalise_corner_angle(gamma);

        debugln!(c, " CE -{} vec={:?} theta={} gamma={}",
                 s.vname(wi), edge_vec, theta, gamma);

        if !prep {
            biggest_gamma = biggest_gamma.max(gamma);
            smallest_gamma = smallest_gamma.min(gamma);
            total_gamma += gamma;
        }

        last_theta = theta;
    }

    let viol = if total_gamma < 1.5 * TAU {
        -smallest_gamma
    } else {
        // total_gamma is TAU*(1 + nbad)
        // where nbad is no. of bad corners
        (TAU - biggest_gamma) + (0.5 * total_gamma - TAU)
        // 2nd term is   TAU/2 * (nbad - 1), which is like
        // assuming that 2nd and further badnesses are 180deg
    };
    debugln!(c, " CV {} viol={}", s.vname(vi), viol);
    viol
}

pub fn edge_length_viols(s : &SolutionRef, viols : &mut [f64]) {
    let c = &s.c;
    let mut viol_dl_i = 0;

    for_face_corners!{ (s, "L";
                        edge_vec, _gamma, vi, wi, _last2_vi, face) {
        if vi < wi {
            let el = edge_vec.len();
            let viol = line_length_target(c, face) - el;
            debugln!(c, "  VL -{} el={} viol={}",
                     wi, el, viol);
            viols[viol_dl_i] = viol;
            viol_dl_i += 1;
        }
    }}
    assert_eq!(viol_dl_i, viols.len());
}

pub fn constraints(s : SolutionRef, viols : &mut [f64]) {
    let c = &s.c;

    for vi in 0..c.edges.len() {
        let viol = vertex_edge_order_viol(&s, vi);
        viols[vi] = viol;
    }
    edge_length_viols(&s, &mut viols[ c.edges.len().. ]);
}

} // end of costmod

//----- support and setup -----

fn print_xs(xs : &[f64]) { // not conditional on any .debug
    print!(" X=");
    for &x in xs { print!(" {:e}",x); }
    print!(" . ");
}

fn setup(g : &PlanarGraph) -> Result<(Vec<f64>,Context)> {
    progressln!(g, "%optim-debug");
    let t = Tuning::new();

    g.set_all_invxs(0);
    let mut faces = Vec::new();
    let mut edges = Vec::with_capacity(g.vs.len());
    for v in &g.vs {
        if v.adj.is_empty() { Err(DisconnectedVertex)? }

        let mut vedges = Vec::new();
        for e in &v.adj {
            let e = EndRef(e);
            vedges.push(e.target());
        }
        vedges.shrink_to_fit();
        edges.push(vedges);

        for face in &v.adj {
            let face = EndRef(face);
            if *em![face,.invx] != 0 { continue }
            let mut vertices = Vec::new();
            for_face!{
                e, (&g,face),
                *em_mut![e,.invx] = 1;
                vertices.push(e.source());
            }
            if vertices.len() <= 1 { Err(FaceWithSingleEdge)? }
            vertices.shrink_to_fit();
            faces.push(vertices);
        }
    }

    let mut t = TuningComputed { input : t, ..Default::default() };

    t.face_interior_angle_cost_factor =
        t.face_interior_angle_at0_cost * t.face_interior_angle_for_halfcost;
    t.line_length_short_factor =
        -t.line_length_at0_cost / t.line_length_target;
    t.line_length_long_factor =
        -t.line_length_short_factor * t.line_length_shrink_strength;
        
    progressln!(g, "# t = {:?}", t);

    let mut xs = Vec::with_capacity(g.vs.len() * 2);
    let mut vnames = Vec::with_capacity(g.vs.len());

    for v in &g.vs {
        let c = v.coordinates.ok_or(MissingStartCoordinate)?;
        for &tc in &c { xs.push(tc) }
        vnames.push(v.name.clone())
    }

    if progressp!(g) {
        progress!(g, " edges=");
        for v in &g.vs {
            for e in &v.adj {
                let e = EndRef(e);
                progress!(g, " {} {}", e.source(), e.target());
            }
        }
        progressln!(g, " .");
        print_xs(&xs);
        progressln!(g, "");
    }

    let c = Context { faces, t, vnames, edges, debug : g.debug };
    Ok((xs, c))
}

fn debrief(g : &mut PlanarGraph, xr : SolutionRef) {
    if progressp!(g) {
        print_xs(xr.xs);
        progressln!(g,"\n%");
    }

    for (vi, v) in g.vs.iter_mut().enumerate() {
        let p = xr.p(vi);
        v.coordinates = Some([p.0, p.1]);
    }
}

impl PlanarGraph {
    pub fn optim_cost(&self) -> Result<(Cost)> {
        let g = self;
        let (xs,c) = setup(g)?;
        let cost = costf(SolutionRef{ xs : &xs, c : &c });
        //print_pos(&x);
        //println!("");
        Ok(cost)
    }

    pub fn optim_viols(&self) -> Result<Vec<f64>> {
        let g = self;
        let (xs,c) = setup(g)?;
        let ncst = nconstraints(&c);
        let mut cst = vec![std::f64::NAN; ncst];
        constraints(SolutionRef { xs : &xs, c : &c }, &mut cst);
        assert!(!cst.iter().any(|x| x.is_nan()));
        Ok(cst)
    }
}

//----- GSL siman -----

#[cfg(feature="siman")]
mod siman {
use super::*;

extern crate rgsl;
use self::rgsl::randist::spherical_vector;
use self::rgsl::types::{Rng,SimAnnealing,SimAnnealingParams};

impl PlanarGraph {
    pub fn anneal(&mut self) -> Result<()> {
        let g = self;
        let mut rng = Rng::new(&rgsl::types::rng::default())
            .ok_or(Error::GslError)?;

        let (xs, c) = setup(g)?;
        let x0 = Solution { c : Rc::new(c), xs };

        let params = SimAnnealingParams::new(
            10, // n_tries
            100, // iters
            0.01, // step_size
            1.0, // k
            0.5, // t_initial
            1.001, // mu_t
            1e-6, // t_min
        );

        fn ef(sv : &Solution) -> f64 {
            let s = SolutionRef { xs : &sv.xs, c : &sv.c };
            costf(s)
        }


        fn print_pos(x : &Solution) { print_xs(&x.xs); }

        fn distance(x : &Solution, y : &Solution) -> f64 {
            let mut sum = 0.;
            for (&xv,&yv) in x.xs.iter().zip( y.xs.iter() ) {
                let d = xv - yv;
                sum += d*d;
            }
            sum.sqrt()
        }

        fn take_step(rng : &mut Rng, x : &mut Solution, size : f64) {
            let mut direction = vec![ 0.; x.xs.len() ];
            spherical_vector::dir_nd(rng, &mut direction);
            for (xv,&dv) in x.xs.iter_mut().zip( direction.iter() ) {
                *xv += dv * size;
            }
        }

        let annealing = SimAnnealing::new(
            x0,
            ef,
            take_step,
            distance,
            if progressp!(g) { Some(print_pos) } else { None },
            params,
        );

        let xr = annealing.solve(&mut rng);

        debrief(g, SolutionRef { xs : &xr.xs, c : &xr.c });

        Ok(())
    }
}

}

// ----- nlopt -----

pub extern crate nlopt;

impl From<nlopt::FailState> for Error {
    fn from(fs : nlopt::FailState) -> Error {
        Error::NLoptFail(fs)
    }
}
impl From<(nlopt::FailState,f64)> for Error {
    fn from(fs_cmin : (nlopt::FailState, f64)) -> Error {
        Error::NLoptFail(fs_cmin.0)
    }
}

impl PlanarGraph {
    pub fn nlopt(&mut self) -> Result<()> {
        let g = self;

        struct AllContext {
            c : Rc<Context>,
            next_progress : Instant,
        };

        let (mut xs, c) = setup(g)?;
        let c = Rc::new(c);

        debugln!(g, "nlopt problem size = {}..", xs.len());

        let algorithm = nlopt::Algorithm::Sbplx;
 
        const PROGRESS_EVERY : Duration = Duration::from_millis(25);

        fn objective(xs : &[f64],
                     dx : Option<&mut [f64]>,
                     ac : &mut AllContext) -> f64 {
            assert!(dx.is_none());
            let c = &ac.c;
            let s = SolutionRef{ xs, c };
            let cost = costf(s);
            if progressp!(c) && (
                c.debug & DEBUG_OPTIM_PROGRESS_ALL !=0 || {
                    let now = Instant::now();
                    let y = now >= ac.next_progress;
                    if y { ac.next_progress = now + PROGRESS_EVERY }
                    y
                }
            ) {
                progress!(c, "cost={} ", cost);
                print_xs(xs);
                progressln!(c, "");
            }
            cost
        }

        fn dummy_objfn(_x : &[f64], _dx : Option<&mut [f64]>,
                       _c : &mut ()) -> f64 { 0. }

        let mut nlo_local = nlopt::Nlopt::new(
            algorithm,
            xs.len(),
            dummy_objfn,
            nlopt::Target::Minimize,
            (),
        );

        nlo_local.set_xtol_abs1(0.0001)?;

        let mut nlo = nlopt::Nlopt::new(
            nlopt::Algorithm::Auglag,
            xs.len(),
            objective,
            nlopt::Target::Minimize,
            AllContext {
                c : c.clone(),
                next_progress : Instant::now() - Duration::from_secs(10),
            },
        );

        nlo.set_local_optimizer(nlo_local)?;

        let mut mins = vec![ 0.; xs.len() ];
        let mut maxs = vec![ 0.; xs.len() ];
        for xyi in 0..=1 {
            let mut min : f64 = 0.;
            let mut max : f64 = 0.;
            for pi in 0..xs.len()/2 {
                let v = xs[ pi*2 + xyi ];
                min = min.min(v);
                max = max.max(v);
            }
            let d = max - min + 2.0;
            debug!(c, " min/max[{}] d={} min={} max={} ", xyi,d,min,max);
            let min = min - d;
            let max = max + d;
            debugln!(c, " => min={} max={} ", min,max);
            for pi in 0..xs.len()/2 {
                let i = pi*2 + xyi;
                mins[i] = min;
                maxs[i] = max;
            }
        }
        nlo.set_lower_bounds(&mins)?;
        nlo.set_upper_bounds(&maxs)?;

        nlo.set_xtol_abs1(0.00005)?;
                
        nlo.set_initial_step1(0.1)?;

        fn mobjfn(result : &mut [f64], xs : &[f64],
                  gradient: Option<&mut [f64]>,
                  c : &mut Rc<Context>) {
            assert!(gradient.is_none());
            constraints(SolutionRef { xs, c }, result);
        }

        debugln!(g, "adding mconstraint {}..", nconstraints(&c));

        nlo.add_inequality_mconstraint(
            nconstraints(&c),
            mobjfn,
            c.clone(),
            &vec![ 0.1 ; nconstraints(&c) ],
        )?;

        debugln!(g, "adding optimising..");

        let (ss, cmin) = nlo.optimize(&mut xs)?;

        println!(" nlopt {:?} {}", ss, cmin);

        debrief(g, SolutionRef { xs : &xs, c : &c });

        Ok(())
    }
}


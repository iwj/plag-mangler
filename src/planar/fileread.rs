// plag-mangler - planar graph handling utility and layout optimiser
//
// fileread.rs
//
//   Copyright (C) 2019 Ian Jackson
//
//  plag-mangler is free software: you can redistribute it and/or modify
//  it under the terms of the GNU Affero General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU Affero General Public License for more details.
//
//  You should have received a copy of the GNU Affero General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.


use super::*;

const DEBUG_FLAG_HERE : DebugFlags = DEBUG_FILEREAD;

struct FrState {
    g : PlanarGraph,
    cvertex : Option<VertexRef>,
    vs : Vec<FrVertex>,
    lno : LineNumber,
}

struct FrVertex {
    ends : HashMap<VertexRef,FrEnd>,
    had_as_v : bool,
    lno : LineNumber,
}

struct FrEnd {
    resolved : bool,
    p : EndRef, // in adj iff resolved
    lno : LineNumber,
}

#[derive(Clone,Debug)] // not Copy since we may put a string in it
pub struct LineNumber (usize);

impl LineNumber {
    #[must_use]
    fn error(&self, s : &str) -> Error {
        Error::Parse(ParseError { lno : self.clone(), msg : s.to_owned() })
    }
}

impl FrState {
    fn err<T>(&self, s : &str) -> Result<T> { Err(self.lno.error(s)) }
}

fn string_dequote(lno : &LineNumber, s : &mut Chars,
                  quote : char /* was before start of s */)
                  -> Result<String>
{
    // Understands the following \-escapes
    //   \\ \' \" \n \r \t \[0-7]{1,3} \xXX \uXXXX \UXXXXXXXX
    let mut out = String::with_capacity(s.as_str().len());
    macro_rules! err { ($s:expr) => {     lno.error($s)  }}
    macro_rules! Err { ($s:expr) => { Err(lno.error($s)) }}

    let mut s = s.peekable();
    // ^ We make an assumption about Peekable here, namely that
    //   it does not consume the inner iterator unnecessarily.
    //   We also impose on the parsing algorithm the requirement
    //   that it most recently called next() rather than peek().
    //   The Peekable api does not provide a way to check that.
    //   We can see that it is true because the only break from
    //   the outside loop is after a next.  (And we don't mind
    //   if parsing errors consume additional character(s))
    loop {
        let c = s.next();
        if c.is_none() { Err!("missing end quote")? }
        let c = c.unwrap();
        if c == quote { break }
        if c != '\\' { out.push(c); continue; }
        let c = s.next().ok_or_else(|| err!("\\ escape at end"))?;

        let mut numeric = |eaten, radix, mut minlen, mut maxlen|
                            -> Result<char> {
            let mut buf = String::with_capacity(maxlen+1);
            let mut expect_end = None;
            if let Some(c) = eaten {
                buf.push(c)
            } else if let Some('{') = s.peek() {
                minlen = 0;
                maxlen = 8;
                expect_end = Some('}');
            }
            loop {
                if buf.len() == maxlen { break }
                let c = s.peek();
                if c.is_none() { break }
                let c = *c.unwrap();
                if !c.is_digit(radix) { break }
                buf.push(c);
            }
            if buf.len() < minlen { Err!("numeric \\ escape val too short")? }
            if buf.len() > maxlen { Err!("numeric \\ escape val too long")? }
            if let Some(ee) = expect_end {
                if s.next() != Some(ee) {
                    Err!(&format!("numeric escape missing final {}", ee))?;
                }
            }
            let cp = u32::from_str_radix(&buf,radix).map_err(
                |e| err!(&format!("invalid integer in numeric escape: {}",e))
            )?;
            let c = std::char::from_u32(cp).ok_or_else(
                || err!("unexpected unicode surrogate")
            )?;
            Ok(c)
        };

        let c = match c {
            't' => '\t',
            'r' => '\r',
            'n' => '\n',
            '\\' | '"' | '\'' => c,
            '0'..='7' => numeric(Some(c),  8,1,3)?,
            'x' =>       numeric(None,    16,1,2)?,
            'u' =>       numeric(None,    16,4,4)?,
            'U' =>       numeric(None,    16,8,8)?,
            _   => Err!("unknown escape")?,
        };
        out.push(c);
    }
    out.shrink_to_fit();
    Ok(out)
}

fn fileread_name(frs : &mut FrState,
                 invertex : Option<VertexRef>,
                 name : &str) -> Result<()> {
    let k = Rc::new( From::from( name ));

    let vi = frs.g.intern.get(&k).map(|&i| i).unwrap_or_else(||{
        let vi = frs.g.vs.len();
        frs.vs.push(FrVertex {
            ends : HashMap::new(),
            had_as_v : false,
            lno : frs.lno.clone(),
        });
        frs.g.vs.push(VertexData {
            adj : List::new(),
            name : k.clone(),
            coordinates : None,
            outer : false,
        });
        frs.g.intern.insert(k, vi);
        vi
    });

    if invertex.is_none() {
        frs.cvertex = Some(vi);
        if frs.vs[vi].had_as_v { frs.err("vertex heading repeated")? }
        frs.vs[vi].had_as_v = true;
        return Ok(());
    }
    let ui = invertex.unwrap();
    if ui == vi { frs.err("self-edge")? }

    // we want to add an edge U-V
    // we know its ordering in U: it is the last edge in U

    // The following situations can arise:
    //    at U         at V
    //      absent       absent         new edge, convert into:
    //      unresolved   resolved       resolve at U
    //      resolved     either         duplicate, error
    // The following situations ought to be impossible:
    //      absent       present
    //      present      absent
    //      unresolved   unresolved

    let [u_vert, v_vert] = slice_index2_mut(&mut frs.vs, [ui,vi]);

    use std::collections::hash_map::Entry::*;
    let u_end = match [u_vert.ends.entry(vi), v_vert.ends.entry(ui)] {
        [Vacant(u_entry), Vacant(v_entry)] => {
            // U will become the A end
            let edge = Rc::new( Edge {
                ll : Default::default(),
                srcs : PerEnd([ui,vi]),
                m : RefCell::new( EdgeMut {
                    added : false,
                    invx : INVXS_INVALID,
                    faceouter : PerEnd([false;2]),
                    facename : PerEnd([None,None]),
                }),
            } );
            let mk_p = |selector|{
                EndRef( Pointer::with_selector(&edge, selector ))
            };
            use self::EndSelector::*;
            let lno = &frs.lno;
            let lno = || lno.clone();
            let u_end = FrEnd { lno:lno(), resolved: true , p: mk_p(A), };
            let v_end = FrEnd { lno:lno(), resolved: false, p: mk_p(B), };
            v_entry.insert(v_end);
            u_entry.insert(u_end) // result
        }
        [Occupied(u_entry), Occupied(v_entry)] => {
            let u_end = u_entry.into_mut();
            let v_end = v_entry.into_mut();
            if u_end.resolved { Err(frs.lno.error("duplicate edge"))? }
            assert!(v_end.resolved);
            u_end.resolved = true;
            u_end
        }
        _ => panic!(),
    };
    assert!(u_end.resolved);
    v!(frs.g, ui).adj.push_back(&u_end.p);
    Ok(())
}

impl PlanarGraph {
    pub fn file_read(&mut self, f : &mut dyn std::io::BufRead)
                     -> Result<PlanarGraph>
    {
        let mut frs = FrState {
            g : PlanarGraph::new(),
            cvertex : None,
            vs : Vec::new(),
            lno : LineNumber(0),
        };
        frs.g.debug = self.debug;

        let mut faceouter = Vec::new();
        let mut faceouter_exact = false;

        loop {
            frs.lno.0 += 1;
            let mut l = String::new();
            f.read_line(&mut l)?;
            if l == "" { break }
            let l = l.trim_end();
            let orglen = l.len(); // lets us tell if it was indented
            let l = l.trim_start();
            if l == "" { continue }
            let mut cs = l.chars();
            let c1 = cs.next().unwrap();
            let rhs = cs.as_str();

            let invertex = {
                if l.len() == orglen { None }
                else if frs.cvertex.is_some() { frs.cvertex }
                else { frs.err("indented data outside first vertex")? }
            };
            let vertex = |lno : &LineNumber, m| -> Result<VertexRef> {
                invertex.ok_or_else(
                    || lno.error(&format!("{} must be in a vertex",m))
                )
            };
            let lno = frs.lno.clone();

            let directive = |rhs|{
                let rhs : &str = rhs;
                // ^ works around a compiler infelicity
                // https://github.com/rust-lang/rust/issues/55526#issuecomment-443216387
                let nonword = rhs.find(
                    |c:char| !(c=='-' || c.is_ascii_alphabetic())
                );
                let (word, rhs) = if let Some(nonword) = nonword {
                    rhs.split_at(nonword)
                } else {
                    (rhs,"")
                };
                let rhs = rhs.trim_left();
                (word, rhs)
            };
            let noargs = |rhs|{
                if rhs == "" { Ok(()) }
                else { Err(lno.error("too many argument(s)")) }
            };
            let nextname = |rhs| -> Result<(String,&str)> {
                let rhs : &str = rhs;
                let mut cs = rhs.chars();
                let c1 = cs.next().ok_or_else(
                    || lno.error("missing argument")
                )?;
                let (name,rhs) = match c1 {
                    '"' => {
                        let name = string_dequote(&lno, &mut cs, c1)?;
                        (name, cs.as_str())
                    },
                    _ if c1.is_alphanumeric() => {
                        let spc = rhs.find(char::is_whitespace);
                        let (name, rhs) =
                            rhs.split_at(spc.unwrap_or(rhs.len()));
                        (name.to_owned(), rhs)
                    },
                    _ => {
                        Err(lno.error("wanted name (quoted or alphanum)"))?
                    },
                };
                let rhs = rhs.trim_start();
                Ok((name,rhs))
            };

            match c1 {
                '#' => continue,
                ':' => {
                    let (word, rhs) = directive(rhs);
                    if word == "face-outer-exact" {
                        noargs(rhs)?;
                        if invertex.is_some() {
                            frs.err(":face-outer-exact is global")?
                        }
                        faceouter_exact = true;
                    } else if word == "outer" {
                        noargs(rhs)?;
                        let vi = vertex(&lno,":outer")?;
                        v!(frs.g,vi).outer = true;
                    } else {
                        frs.err("unknown :-directive")?
                    }
                },
                '^' => {
                    let (word, rhs) = directive(rhs);
                    let vi = vertex(&lno,"^-directive")?;
                    let mut e = v!(frs.g,vi).adj.last().ok_or_else(
                        || lno.error("^-directive must follow an edge")
                    )?;
                    if word == "added" {
                        noargs(rhs)?;
                        let mut m = e.m.borrow_mut();
                        m.added = true;
                    } else if word == "lface-outer" {
                        // applies to face to left of this edge
                        // edges listed anticlockwise
                        // so applies to face after this edge,
                        // which is where the directive is
                        noargs(rhs)?;
                        faceouter.push((frs.lno.clone(), EndRef(e).clone()));
                    } else if word == "lface" {
                        let (name,rhs) = nextname(rhs)?;
                        noargs(rhs)?;
                        let mut was = em_mut![e,.facename];
                        match *was {
                            None => *was = Some(Rc::new(name)),
                            _ => frs.err("repeated lface")?,
                        }
                    } else {
                        frs.err("unknown ^-directive")?
                    }
                },
                '\"' => {
                    let mut rhs = rhs.chars();
                    let name = string_dequote(&lno, &mut rhs, c1)?;
                    if rhs.next().is_some() {
                        Err(lno.error("junk ({:?}) after string"))?;
                    }
                    fileread_name(&mut frs, invertex, &name)?;
                },
                '@' => {
                    let vi = vertex(&lno,"@")?;
                    if v!(frs.g,vi).coordinates.is_some() {
                        frs.err("duplicate coordinates")?
                    }
                    let mut coords = [0.;2];
                    for (i,v) in rhs
                        .split(',')
                        .map(|s| s.trim().parse())
                        .enumerate()
                    {
                        if i > 2 { frs.err("expected only 2 coordinates")? }
                        coords[i] = v.map_err(
                            |pe| frs.lno.error(
                                &format!("bad coordinate: {:?}",pe)
                            )
                        )?;
                    }
                    v!(frs.g,vi).coordinates = Some(coords);
                }
                _ => {
                    if c1.is_ascii_punctuation() {
                        frs.err("unknown punctuation")?
                    } else if c1.is_alphanumeric() {
                        fileread_name(&mut frs, invertex, l)?;
                    } else {
                        frs.err("unknown character")?
                    }
                }
            }
        }

        for v in &frs.vs {
            if !v.had_as_v {
                Err(v.lno.error("target-only vertex"))?
            }
            for (_, v_end) in &v.ends {
                if !v_end.resolved {
                    Err(v_end.lno.error("unidrectional edge"))?
                }
            }
        }

        for (_, e) in &mut faceouter {
            debugln!(frs.g,"FACEOUTER SET {}", e.debug(&frs.g));
            *em_mut![e,.faceouter] = true;
        }
        for (lno, e) in &mut faceouter {
            debugln!(frs.g,"FACEOUTER FRM {}", e.debug(&frs.g));
            for_face!{
                u, (&frs.g, e),
                debugln!(frs.g,"FACEOUTER ATT {}", u.debug(&frs.g));
                let mut m = u.m.borrow_mut();
                let os = &mut m.faceouter[ u.selector() ];
                if faceouter_exact {
                    if !*os { Err(lno.error("inexact faceouter"))? }
                } else {
                    *os = true;
                }
            }
        }

        Ok(frs.g)
    }
}

// plag-mangler - planar graph handling utility and layout optimiser
//
// filewrite.rs
//
//   Copyright (C) 2019 Ian Jackson
//
//  plag-mangler is free software: you can redistribute it and/or modify
//  it under the terms of the GNU Affero General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU Affero General Public License for more details.
//
//  You should have received a copy of the GNU Affero General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.


use super::*;

pub type Writer<'w> = &'w mut dyn std::io::Write;

//----- File writing -----

pub fn write_quoted(f : Writer, s : &str) ->io::Result<()>{
    for c in s.chars() {
        match c {
            '\\' => write!(f, "\\\\")?,
            '\n' => write!(f, "\\n")?,
            '"'  => write!(f, "\\\"")?,
            '\0' => write!(f, "\\000")?,
            '\x01'..='\x1f' => write!(f, "\\x{:2x}", c as u8)?,
            _    => write!(f, "{}", c)?,
        }
    }
    Ok(())
}

fn writename(f : Writer, name : &str) ->io::Result<()>{
    write!(f,"\"")?;
    write_quoted(f, name)?;
    write!(f,"\"")?;
    Ok(())
}

fn write1name(f : Writer,
              prefix : &str, name : &str) ->io::Result<()>{
    write!(f,"{}\"", prefix)?;
    write_quoted(f, name)?;
    writeln!(f,"\"")?;
    Ok(())
}

fn write1coords(f : Writer,
                prefix : &str,
                coordinates : Option<[Coordinate;2]>) -> io::Result<()> {
    if let Some(ref c) = coordinates {
        writeln!(f,"{}@{},{}", prefix, c[0], c[1])?;
    }
    Ok(())
}

impl PlanarGraph {
    pub fn file_write(&self, f : Writer) -> io::Result<()> {
        let g = self;
        for v in &g.vs {
            write1name(f,"",&v.name)?;
            write1coords(f," ",v.coordinates)?;
            if v.outer {
                writeln!(f," :outer");
            }
            for e in &v.adj {
                let e = EndRef(e);
                write1name(f, " ", &v!(g,e,target).name)?;
                if *em![e,.faceouter] { writeln!(f," ^lface-outer")?; }
                let facename = em![e,.facename];
                if let Some(ref facename) = *facename {
                    write1name(f," ^lface ",facename)?;
                }
            }
        }
        Ok(())
    }

    pub fn faces_write(&self, f : Writer) ->Result<()> {
        let g = self;
        g.set_all_invxs(0);
        let mut fintern = HashMap::new();
        writeln!(f,"&faces")?;
        for v in &g.vs {
            for face in &v.adj {
                if *em![face,.invx] != 0 { continue }
                let face = EndRef(face);
                let mut names = Vec::new();
                let mut hadname = HashSet::new();
                let mut outer = 0;
                let mut nedges = 0;
                for_face!{
                    e, (g,face),
                    if let Some(ref facename) = *em![e,.facename] {
                        if hadname.insert(facename.clone()) {
                            names.push(facename.clone());
                        }
                    }
                    if *em![e,.faceouter] {
                        outer += 1;
                    }
                    nedges += 1;
                }
                let facename = intern_uniquely_parts
                    (&mut fintern, '|',"",0, &names);
                write1name(f, "", &facename)?;
                writeln!(f, " :n-edges {}", nedges);
                writeln!(f, " :edges-outer {} {}", outer, match outer {
                    0                  => "none",
                    _ if outer==nedges => "all",
                    _                  => "some",
                });
                for_face!{
                    e, (g,face),
                    write1name(f, " ", &v!(g,e,source).name)?;
                    write1coords(f, " ^", v!(g,e,source).coordinates)?;
                    let erev = e.reverse();
                    if let Some(ref facename) = *em![erev,.facename] {
                        write1name(f, " ^adjoins ", facename)?;
                    }
                    *em_mut![e,.invx] = 1;
                }
            }
        }
        writeln!(f,"&")?;
        Ok(())
    }

    pub fn graphviz_write(&self,
                          f : Writer,
 mut vxinfo : Option< &mut FnMut(Writer, VertexRef )
                                 -> io::Result<()> >,
 mut exinfo : Option< &mut FnMut(Writer, [VertexRef;2],
                                 &EdgeInfo) -> io::Result<()> >,
                          ) -> io::Result<()>
    {
        let g = self;
        if exinfo.is_some() { g.ensure_invxs(); }
        for (vi,v) in g.vs.iter().enumerate() {
            writename(f,&v.name)?;
            write!(f, " [")?;
            if let Some(ref mut vxinfo) = vxinfo { vxinfo(f,vi)?; }
            write!(f, "];\n")?;
            for e in &v.adj {
                let e = EndRef(e);
                if e.selector() == EndSelector::B { continue }
                writename(f, &v.name)?;
                assert_eq!(vi, e.source());
                write!(f," -- ")?;
                writename(f, &v!(g,e,target).name)?;
                write!(f, " [")?;
                if let Some(ref mut exinfo) = exinfo {
                    exinfo(f, e.srcs.0, &EdgeInfo(&*e.m.borrow()))?;
                }
                write!(f, "];\n")?;
            }
        }
        Ok(())
    }
}

// plag-mangler - planar graph handling utility and layout optimiser
//
// mod.rs - top level module for plag_mangler::planer
//
//   Copyright (C) 2019 Ian Jackson
//
//  plag-mangler is free software: you can redistribute it and/or modify
//  it under the terms of the GNU Affero General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU Affero General Public License for more details.
//
//  You should have received a copy of the GNU Affero General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.


use arrayvec::ArrayVec;

use std::collections::hash_map::{self,HashMap};
use std::collections::hash_set::HashSet;
use std::collections::vec_deque::VecDeque;
use std::rc::Rc;
use std::cell::{self,RefCell};
use std::fmt::{self,Write,Formatter};
use std::io;
use std::str::Chars;
use std::mem::replace;
use std::iter;

//#[macro_use] <- it's the one in main.rs that takes effect!
use rc_dlist_deque::dlist as list;
use self::list::{Link,List,Pointer};

//----- primary data structures -----

pub type DebugFlags = u32;
pub const DEBUG_GENERAL              : DebugFlags = 0x00000001;
pub const DEBUG_DUAL                 : DebugFlags = 0x00000002;
pub const DEBUG_FILEREAD             : DebugFlags = 0x00000004;
pub const DEBUG_AUGMENT              : DebugFlags = 0x00000008;
pub const DEBUG_OUTER                : DebugFlags = 0x00000010;
pub const DEBUG_OPTIM_PROGRESS       : DebugFlags = 0x00001000;
pub const DEBUG_OPTIM_PROGRESS_ALL   : DebugFlags = 0x00004000;
pub const DEBUG_OPTIM_COST           : DebugFlags = 0x00002000;

pub const DEBUG_FLAGS_DEFAULT : DebugFlags = (
    DEBUG_OPTIM_PROGRESS |
    0 );

#[derive(Default)]
pub struct PlanarGraph {
    vs : Vec<VertexData>,
    intern : HashMap<Rc<String>, usize>,
    debug : DebugFlags,
}

struct VertexData {
    adj : List<Edge,EndSelector>,
    name : Rc<String>,
    outer : bool,
    coordinates : Option<[Coordinate;2]>,
}

struct Edge {
    ll : PerEnd< Link<Edge, EndSelector> >,
    srcs : PerEnd< VertexRef >,
    /* ll[0] is in the list for vertex srcs[0] */
    m : RefCell<EdgeMut>,
}

type Invx = usize;

#[derive(Clone)]
struct EdgeMut {
    added : bool,
    invx : PerEnd< Invx >, // valid => this is invx[]'th in src[]
    faceouter : PerEnd< bool >,
    facename : PerEnd<Option<Rc<String>>>,
    // face to left of this direction
}

pub struct EdgeInfo<'r> (&'r EdgeMut);

pub type VertexRef = usize;
pub type Coordinate = f64;

//----- Error etc. -----

#[derive(Debug)]
pub enum Error {
    IO( std::io::Error ),
    Parse( ParseError ),
    InputStructure(InputStructureError),
    #[allow(dead_code)] CallerError,
    #[allow(dead_code)] GslError,
    VertexNameClash,
    NLoptFail(optimise::nlopt::FailState),
}

#[derive(Debug)]
pub struct ParseError {
    msg : String,
    lno : LineNumber,
}

#[allow(dead_code)]
#[derive(Debug)]
#[must_use]
pub enum InputStructureError {
    GraphTooSmall,
    DisconnectedVertex,
    OuterUnspecified,
    OuterInconsistent,
    PCONotBiconnected,
    PCOIncomplete,
    PCOBaseNotReady,
    DualGivesMultipleEdge,
    AugmentGivesSelfEdge,
    FaceWithSingleEdge,
    MissingStartCoordinate,
}

type Result<T> = std::result::Result<T,Error>;

use self::InputStructureError::*;

impl From<io::Error> for Error { fn from(x:io::Error)->Error{ Error::IO(x) }}
impl From<InputStructureError> for Error { fn from(x : InputStructureError)->Error{ Error::InputStructure(x) }}

//----- end selector and ref machinery -----

#[derive(Copy,Clone,Debug,Eq,PartialEq,Ord,PartialOrd)]
enum EndSelector { A, B }

#[derive(Debug,Clone)] // We regard the selected end as the source
struct EndRef (Pointer<Edge,EndSelector>);

#[derive(Copy,Clone,Debug,Default)]
struct PerEnd<T> ([ T ; 2 ]);

const ENDS : [EndSelector;2] =
    [ EndSelector::A, EndSelector::B ];

impl<T> std::ops::Index   <EndSelector> for PerEnd<T> {
    type Output = T;
 fn index    (&    self,ab:EndSelector)->&    T { &    self.0[ab as usize] }
}
impl<T> std::ops::IndexMut<EndSelector> for PerEnd<T> {
 fn index_mut(&mut self,ab:EndSelector)->&mut T { &mut self.0[ab as usize] }
}

impl<T> std::convert::From<[T;2]> for PerEnd<T> {
    fn from(a : [T;2]) -> PerEnd<T> { PerEnd(a) }
}

impl std::ops::Not for EndSelector {
    type Output = EndSelector;
    fn not(self) -> Self {
        use self::EndSelector::*;
        match self { A => B, B => A }
    }
}

impl std::ops::Deref for EndRef {
    type Target = Pointer<Edge,EndSelector>;
    fn deref(&self) -> &Pointer<Edge,EndSelector> { &self.0 }
}
impl std::ops::DerefMut for EndRef {
    fn deref_mut(&mut self) -> &mut Pointer<Edge,EndSelector> { &mut self.0 }
}

impl std::cmp::PartialEq for EndRef {
    fn eq(&self, other : &EndRef) -> bool {
        Pointer::ptr_eq(&self.0, &other.0)
    }
}
impl std::cmp::Eq for EndRef { }

DlistImplSelector!( EndSelector, Edge, s [ .ll[s] ]);

impl fmt::Display for EndSelector {
    fn fmt(&self, f : &mut Formatter) -> fmt::Result {
        use self::EndSelector::*;
        write!(f,"{}", match self { A => "A", B => "B" })
    }
}
impl list::SelectorFmt for EndSelector {
    fn fmt(&self, f : &mut Formatter) -> fmt::Result {
        write!(f,"-{}",self)
    }
}

//----- macros -----

const DEBUG_FLAG_HERE : DebugFlags = DEBUG_GENERAL;

macro_rules! debugp { ($g:expr) => ($g.debug & DEBUG_FLAG_HERE != 0) }
macro_rules! debug {
    ($g:expr, $($rhs:tt)*) => { (if debugp!($g) {   eprint!($($rhs)*); }) }
}
macro_rules! debugln {
    ($g:expr, $($rhs:tt)*) => { (if debugp!($g) { eprintln!($($rhs)*); }) }
}

macro_rules! v {
    ($g:expr, $vr:expr) => ( $g.vs[$vr] );
    ($g:expr, $e:expr, $sd:ident) => ( $g.vs[$e.$sd()] );
}

#[allow(unused_macros)]
macro_rules! e {
    [ $endref:expr, $($dotperend:tt)* ] => (
        {
            let s = $endref.selector();
            $endref $($dotperend)* [ s ]
        }
    );
}
macro_rules! em {
    [ $endref:expr, $($dotperend:tt)* ] => ({
        let s = $endref.selector();
        cell::Ref::map( $endref.m.borrow(),
                        |m| & m $($dotperend)* [ s ] )
    })
}
macro_rules! em_mut {
    [ $endref:expr, $($dotperend:tt)* ] => ({
        let s = $endref.selector();
        cell::RefMut::map( $endref.m.borrow_mut(),
                           |m| &mut m $($dotperend)* [ s ] )
    })
}

macro_rules! for_face {
    { $e:ident, ($g:expr, $start:expr),
      $($body:tt)*} => {
        {
            let mut iter = FaceIterator::new(&$start);
            #[allow(unused_mut)]
            while let Some(mut $e) = iter.next($g)? {
                $($body)*
            }
        }
    }
}

//----- random utilities -----

fn slice_index2_mut<T>(ary : &mut [T], mut i : [ usize; 2 ])
                       -> [ &mut T; 2 ] {
    // returns mutable references to two slice elments
    assert_ne!(i[0], i[1]);
    let swap = i[0] > i[1];
    if swap { i.reverse(); }
    let (l,r) = ary.split_at_mut(i[1]);
    let mut out = [ &mut l[i[0]], &mut r[0] ];
    if swap { out.reverse(); }
    out
}

const INVX_INVALID : usize = !0;
const INVXS_INVALID : PerEnd<usize> = PerEnd( [INVX_INVALID;2] );

impl fmt::Display for VertexData {
    fn fmt(&self, f : &mut Formatter) -> fmt::Result {
        write!(f,"{}", self.name)
    }
}
//----- nontrivial code for EndRef etc. -----

impl EndRef {
    fn new(p : &list::Pointer<Edge,EndSelector>) -> Self { EndRef(p.clone()) }
    fn source(&self) -> VertexRef { self.srcs[ self.selector()] }
    fn target(&self) -> VertexRef { self.srcs[!self.selector()] }
    fn reverse(&self) -> EndRef { EndRef(
        list::Pointer::with_selector(self, !self.selector())
    )}

    fn debug(&self, g : &PlanarGraph) -> String {
        let e = self;
        let m = e.m.borrow();
        let dinvx = |s| {
            let i = m.invx[s];
            if i == !0 { format!("X") }
            else { format!("{}",i) }
        };
        format!("{}-{}_({}{}-{}{})",
                v!(g,e,source).name, v!(g,e,target).name,
                dinvx( !e.selector() ), e.selector(),
                dinvx(  e.selector() ),
                { if m.added { "+" } else { "" } })
    }
}

fn next_edge(g : &PlanarGraph, e : &EndRef, rev : bool)
             -> Result<EndRef> {
// eprintln!("next_edge(,,{}) {} ...\n  H={:?} T={:?}", rev, e.debug(g),
//           v!(g,e,source).adj.first_last(false),
//           v!(g,e,source).adj.first_last(true),
//     );
// {
//     for ee in &v!(g,e,source).adj {
//         let x = ee.clone();
//         eprintln!("  F {}\n  P={:?} N={:?}", EndRef(ee).debug(g),
//                   x.iter_at().next_prev(true),
//                   x.iter_at().next_prev(false),
//         );
//     }
//     let mut ee = v!(g,e,source).adj.start();
//     while ee.is_some() {
//         eprintln!("  f {}", EndRef(ee.clone().unwrap()).debug(g));
//         ee.walk(false);
//     }
//     let mut ee = v!(g,e,source).adj.end();
//     while ee.is_some() {
//         eprintln!("  R {}", EndRef(ee.clone().unwrap()).debug(g));
//         ee.walk(true);
//     }
// }

    let r = EndRef::new(
        &e
            .get_next_prev(rev)
            .map(Ok).unwrap_or(
//{
//eprintln!("next_edge(,,{}) {} restarts", rev, e.debug(g));
                v!(g,e,source)
                    .adj.first_last(rev)
                    .ok_or(DisconnectedVertex)
//}
            )?
    );
//eprintln!("next_edge(,,{}) {} => {}", rev, e.debug(g), r.debug(g));
    Ok(r)
}

fn face_next_edge(g : &PlanarGraph, e : &EndRef) -> Result<EndRef> {
    // anticlockwise around the face to the left of e
    Ok( next_edge(g, &e.reverse(), true)? )
}

fn face_prev_edge(g : &PlanarGraph, e : &EndRef) -> Result<EndRef> {
    // clockwise around the face to the left of e
    Ok( next_edge(g, e, false)?.reverse() )
}

//----- face iterator -----

struct FaceIterator {
    s : EndRef,
    e : Option<EndRef>,
}

// can't be an Iterator because next needs g :-/, and because can fail

impl FaceIterator {
    fn new(s : &EndRef) -> FaceIterator { FaceIterator {
        s : s.clone(),
        e : Some(s.clone()),
    }}
    fn next(&mut self, g : &PlanarGraph) -> Result<Option<EndRef>> {
        if self.e.is_none() { return Ok(None) }
        let n = face_next_edge(g, self.e.as_ref().unwrap())?;
        let n = if n == self.s { None } else { Some(n) };
        let r = replace(&mut self.e, n);
        Ok(r)
    }
}

//----- utilities -----

fn add_edge(g : &mut PlanarGraph, /* joins sources of ends */
            added : bool,
            ends : PerEnd<( &EndRef, bool /* after */ )>,
            faceouter : PerEnd< bool >,
            facename : PerEnd<Option<Rc<String>>>,)
            -> Result<PerEnd<EndRef>>
{
    const DEBUG_FLAG_HERE : DebugFlags = DEBUG_AUGMENT;

    /* entry `false' will become the A end of the new edge */
    let mut srcs = PerEnd([0;2]);
    debug!(g,"  ADD_EDGE");
    for &ab in &ENDS {
        srcs[ab] = ends[ab].0.source();
        debug!(g," {:?}", v!(g,srcs[ab]).name);
    }
    debugln!(g,"");
    if srcs[EndSelector::A] == srcs[EndSelector::B] {
        Err(AugmentGivesSelfEdge)?
    }
    let edge = Rc::new( Edge {
        ll : Default::default(),
        srcs,
        m : RefCell::new( EdgeMut {
            added,
            invx : INVXS_INVALID,
            faceouter,
            facename,
        }),
    });

    let mut out = ArrayVec::new();
    for &ab in &ENDS {
        let source = edge.srcs[ab];
        let p = EndRef::new( &Pointer::with_selector(&edge,ab) );
        v!(g, source).adj
            .insert(&p, &ends[ab].0, ends[ab].1);
        out.push(p);
    }
    Ok(PerEnd( out.into_inner().unwrap() ))
}

fn add_edge_within(g : &mut PlanarGraph, /* joins sources of ends */
                   added : bool,
                   ends : PerEnd<( &EndRef, bool /* after */ )>,
                   org_face : &EndRef)
                   -> Result<PerEnd<EndRef>>
{
    let faceouter = PerEnd([ *em![org_face,.faceouter] ;2 ]);
    let facename = PerEnd(
        ArrayVec::into_inner(
            std::iter::repeat(())
                .take(2)
                .map(|_| em![org_face,.facename].clone())
                .collect()
        ).unwrap()
    );
    add_edge(g,added,ends, faceouter,facename)
}

fn intern_uniquely_parts(intern : &mut HashMap<Rc<String>,usize>,
                         namesep : char,
                         suffix : &str,
                         vi : usize,
                         // ^ all as for intern_uniquely
                         parts : &Vec<Rc<String>>)
                         -> Rc<String> {
    let mut q = String::new();
    for (delim, p) in iter::once(false) .chain(iter::repeat(true))
        .zip(parts)
    {
        if delim { write!(q, " {} ", namesep).unwrap() }
        for c in p.chars() {
            q.push(c);
            if c == namesep { q.push(c); }
        }
    }
    intern_uniquely(intern,namesep,suffix, vi,q)
}

fn intern_uniquely(intern : &mut HashMap<Rc<String>,usize>,
                   namesep : char,
                   suffix : &str, // empty or ascii [alpnaumerics]letters
                   // ^ context
                   vi : usize,
                   name_base : String) -> Rc<String> {
    for sequence in 0.. {
        let nname = Rc::new(
            if sequence == 0 && suffix == "" {
                name_base.clone()
            } else if sequence == 0 {
                format!("{} {}{}", name_base, namesep, suffix)
            } else {
                format!("{} {}{}{}", name_base, namesep, suffix, sequence)
            }
        );
        let e = intern.entry(nname.clone());
        if let hash_map::Entry::Vacant(e) = e {
            e.insert(vi);
            return nname;
        }
    }
    panic!();
}

//----- key public methods -----

impl PlanarGraph {
    pub fn new() -> Self { PlanarGraph {
        vs : Vec::new(),
        intern : HashMap::new(),
        debug : DEBUG_FLAGS_DEFAULT,
    } }

    pub fn set_debug(&mut self, flags : DebugFlags) { self.debug = flags }

    pub fn nvertices(&self) -> usize { self.vs.len() }
    #[allow(dead_code)]
    pub fn name2vi(&self, name : &str) -> Option<usize> {
        self.intern.get(&name.to_owned()).cloned()
    }
    pub fn vi2name   (&self, vi:usize) -> &Rc<String> { &self.vs[vi].name   }
    pub fn vi2coords (&self, vi:usize) -> Option<[Coordinate;2]> {
        self.vs[vi].coordinates
    }
    pub fn vi2outer (&self, vi:usize) -> bool { self.vs[vi].outer }

    pub fn vi_set_coords(&mut self, vi:usize, c : Option<[Coordinate;2]>) {
        self.vs[vi].coordinates = c;
    }
    #[allow(dead_code)]
    pub fn vi_set_name(&mut self, vi:usize, n : Rc<String>) -> Result<()> {
        match self.intern.entry(n.clone()) {
            hash_map::Entry::Vacant(e) => {
                self.vs[vi].name = n;
                e.insert(vi);
                Ok(())
            },
            hash_map::Entry::Occupied(_) => {
                Err(Error::VertexNameClash)
            }
        }
    }
    pub fn vs_set_names_all(&mut self, names : &[Rc<String>]) -> Result<()> {
        assert_eq!(names.len(), self.vs.len());
        let mut intern = HashMap::new();
        for (i,n) in names.iter().enumerate() {
            if intern.insert(n.clone(), i).is_some() {
                Err(Error::VertexNameClash)?
            }
        }
        for (i,n) in names.iter().enumerate() {
            self.vs[i].name = n.clone();
        }
        self.intern = intern;
        Ok(())
    }
}

impl<'r> EdgeInfo<'r> {
    pub fn added(&self) -> bool { self.0.added }
    pub fn invx(&self) -> [usize;2] { self.0.invx.0 }
    pub fn faceouter(&self) -> [bool;2] { self.0.faceouter.0 }
    pub fn facename(&self, side : usize) -> Option<Rc<String>> {
        self.0.facename.0[side].clone()
    }
}

impl PlanarGraph {
    pub fn triangulate(&mut self) -> Result<()> {
        // [Kant 1993] Goossen Kant
        //  _Algorithms for Drawing Planar Graphs_
        // (_Algoritmen voor het Tekenen van Planaire Grafen+)
        // Proefschrift, Rijksuniversiteit te Utrecht, 1993.
        //
        // we implement the triangulation Algorithm described briefly
        // at p85 of the pdf (labelled p75) (chapter 6) attributed to
        // Hagerup & Uhrig [44].  Unfortunately Hagerup and Uhrig's
        // algorithm seems to be published only in the form of a
        // proprietary software package, so we go only by Kant.

        //assert!(self.connected.unwrap());
        let g = self;
        if g.vs.len() < 3 { Err(GraphTooSmall)? }
        let mut marked = vec![ false; g.vs.len() ];
        for vi in 0..g.vs.len() {
            let markall = |g : &PlanarGraph, marked : &mut Vec<bool>, value| {
                for e in &v!(g,vi).adj {
                    let e = EndRef(e);
                    marked[ e.target() ] = value;
                }
            };
            markall(g, &mut marked, true);
            // we modify the adj list for vi, so we need to open-code
            // the iteration so we can adjust the iterator appropriately
            let mut e_iter = v!(g,vi).adj.iter();
            while let Some(e) = e_iter.cursor() {
                let u1 = &EndRef(e);
                e_iter.walk(false);

                // u1 is the face (to the left of the edge); also
                // each of u1,... represents the vertex u1.source() etc.
                let u2 = face_next_edge(g,&u1)?;
                assert_ne!(*u1, u2);
                let u3 = face_next_edge(g,&u2)?;
                let u4 = face_next_edge(g,&u3)?;
                if u4 == *u1 { continue } // triangle

                if !marked[ u3.source() ] {
                    marked[ u3.source() ] = true;
                    let PerEnd([u1u3,_]) = add_edge_within(g, true, PerEnd([
                        (  u1, true ),
                        ( &u3, true ),
                    ]), u1 )?;
                    e_iter = u1u3.iter_at(); // continue with u1,u3,...
                } else {
                    add_edge_within(g, true, PerEnd([
                        ( &u2, true ),
                        ( &u4, true ),
                    ]), u1 )?;
                    e_iter = u1.iter_at(); // continue with u1,u2,..
                }
            }
            markall(g, &mut marked, false);
        }
        Ok(())
    }

    fn ensure_invxs(&self) {
        let g = self;
        for v in &g.vs {
            for (ei, e) in v.adj.iter().enumerate() {
                let e = EndRef(e);
                *em_mut![e,.invx] = ei;
            }
        }
    }

    pub fn remove_added_edges(&mut self) {
        let g = self;
        for vi in 0..g.vs.len() {
            for e in &v!(g,vi).adj {
                let e = EndRef(e);
                if !e.m.borrow().added { continue }
                for e in &[e.reverse(), e] {
                    v!(g,e,source).adj.remove(&e);
                }
            }
        }
    }
}

impl PlanarGraph {
    fn edge_xref_storage<T:Clone>(&self, init : T) -> (usize, Vec<Vec<T>>) {
        // return is (edge_count, xs) where
        // xs[ vi ][ some edge in e inxs ] = None
        // Caller must have called ensure_invxs
        let g = self;
        let mut xs : Vec<Vec<T>> = Vec::new();
        let mut edge_count = 0;
        for v in &g.vs {
            let adj_count = v.adj.last().map_or(0, |e| *em![e,.invx] + 1 );
            xs.push(vec![init.clone(); adj_count]);
            edge_count += adj_count;
        }
        (edge_count, xs)
    }

    fn set_all_invxs(&self, val : Invx) {
        let g = self;
        for v in &g.vs {
            for mut e in &v.adj {
                *em_mut![e,.invx] = val;
            }
        }
    }
}

impl Clone for PlanarGraph {
    fn clone(&self) -> Self {
        let g = self;
        g.ensure_invxs();
        let mut vs = Vec::new();
        let (_, mut xs) = g.edge_xref_storage::<Option<EndRef>>(None);
        for v in &g.vs {
            vs.push(VertexData {
                adj : List::new(),
                name : v.name.clone(),
                coordinates : v.coordinates,
                outer : v.outer,
            });
//eprintln!("CLONE PREP {} {}", v, adj_count);
        }
        for v in &g.vs {
//eprintln!("CLONE ADD {}", v);
            for e in &v.adj {
                let e = EndRef(e);
//eprintln!("CLONE ADD {}", e.debug(g));
                if e.selector() == EndSelector::B { continue }
                let m = e.m.borrow();
                let ne = Rc::new( Edge { 
                        ll : Default::default(),
                        srcs : e.srcs,
                        m : RefCell::new(m.clone()),
                });
//eprintln!("CLONE ADD {} srcs={:?}", e.debug(g), ne.srcs);
                for &end in &ENDS {
                    let e = EndRef::new( &Pointer::with_selector(
                        &ne,
                        end,
                    ));
//eprintln!("CLONE ADD {} srcs={:?} {} source={} invx={}", e.debug(g), ne.srcs,
//          end, e.source(), m.invx[end]);
                    xs[ e.source() ][ m.invx[end] ] = Some(e.clone());
                }
            }
        }
        for (v,x) in vs.iter_mut().zip(&xs) {
//eprintln!("ADJ {}", &v);
            for ne in x {
                let ne = ne.as_ref().expect("bidirectional coherent");
//eprintln!("ADJ {} {} {:?} {}", &v, ne.debug(g),
//                          ne.srcs, ne.selector());
                v.adj.push_back(ne);
            }
        }
        let g = PlanarGraph {
            vs,
            intern : g.intern.clone(),
            debug : g.debug,
        };

        #[cfg(debug_assertions)]
        for (vi, v) in g.vs.iter().enumerate() {
            for e in &v.adj {
                let e = EndRef(e);
//eprintln!("CHECCK #{} {} srcs={:?} {}", vi, &v, e.srcs, e.selector());
                assert_eq!( e.source(), vi );
            }
        }
        g
    }
}

//----- submodules -----

// these have to be after the macros, so put them last

mod fileread;
pub use self::fileread::*;

mod filewrite;
pub use self::filewrite::*;

mod outer;

mod pco;
pub use self::pco::*;

mod biconnected;
pub use self::pco::*;

mod chrobakpayne;
mod dual;

mod coord;
mod optimise;


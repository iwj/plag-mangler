// plag-mangler - planar graph handling utility and layout optimiser
//
// dual.rs - computing the dual
//
//   Copyright (C) 2019 Ian Jackson
//
//  plag-mangler is free software: you can redistribute it and/or modify
//  it under the terms of the GNU Affero General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU Affero General Public License for more details.
//
//  You should have received a copy of the GNU Affero General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.


use super::*;

//----- dual -----

const DEBUG_FLAG_HERE : DebugFlags = DEBUG_DUAL;

// we abuse invxs grievously
// invxs[A] is flags saying which faces we have done
// invxs[B] is the index into our array of new edges

#[allow(dead_code)] const INVX_FACEDONE_A    : Invx = 0x0001;
#[allow(dead_code)] const INVX_FACEDONE_B    : Invx = 0x0002;

fn done_flag(e : &EndRef) -> usize { 1 << (e.selector() as u8) }

impl PlanarGraph {
    pub fn dual(&self, namesep : char) -> Result<PlanarGraph> {
        // We walk the faces in the input graph, building an array of
        // vertices.  The face to the left of each old edge becomes
        // the source vertex for the new edge.
        let g = self;
        g.set_all_invxs(0);
        let mut nvs = Vec::new();
        let mut nvx = Vec::new();
        let mut nedges = Vec::new();
        let mut intern = HashMap::new();
        for v in &g.vs {
            for e0 in &v.adj {
                let e0 = EndRef(e0);
                if e0.m.borrow().invx[EndSelector::A] & done_flag(&e0) != 0 {
                    debugln!(g, "DUAL u {} already", e0.debug(g));
                    continue
                }

                let nvi = nvs.len();
                let mut face_n_edges = 0;
                for_face!{ _e, (g,e0), face_n_edges += 1 };
                let mut nameparts = Vec::with_capacity(face_n_edges);
                let mut name_from_face_done = None;
                let mut nv_edges = Vec::with_capacity(face_n_edges);
                let mut outer = false;
//            let nv_edges : Vec<Vec<(usize,EndSelector)>> = Vec::new();
                for_face!{
                    e, (g,e0),

                    let nei = {
                        debug!(g,"DUAL {} : {} ", e0.debug(g), e.debug(g));
                        let mut m = e.m.borrow_mut();
                        let done = m.invx[EndSelector::A];
                        debug!(g,"done/flag={}/{} ", done, done_flag(&e));
                        if done & done_flag(&e) != 0 {
                            debugln!(g,"already");
                            continue;
                        }
                        m.invx[EndSelector::A] |= done_flag(&e);
                        if done != 0 {
                            let r = m.invx[EndSelector::B];
                            debug!(g,"2nd {}", r);
                            r
                        } else {
                            let ne = Edge {
                                ll : Default::default(),
                                srcs : PerEnd([!0;2]),
                                m : RefCell::new(EdgeMut {
                                    added : m.added,
                                    invx : INVXS_INVALID,
                                    faceouter : PerEnd([false;2]),
                                    facename : PerEnd([None,None]),
                                }),
                            };
                            let nei = nedges.len();
                            nedges.push(ne);
                            m.invx[EndSelector::B] = nei;
                            debug!(g,"1st {}", nei);
                            nei
                        }
                    };

                    nedges[nei].srcs[ e.selector() ] = nvi;
                    {
                        let mut m = nedges[nei].m.borrow_mut();
                        let s = !e.selector();
                        m.facename[s] = Some(v!(g,e,source).name.clone());
                        m.faceouter[s] = v!(g,e,source).outer;
                    }
                        
                    nv_edges.push(( nei, e.selector() ));

                    outer |= *em![e,.faceouter];

                    let namepart = {
                        if let Some(ref facename) = *em![e,.facename] {
                            debug!(g," f={}", facename);
                            if name_from_face_done == None {
                                debug!(g," resetting");
                                nameparts.clear();
                                name_from_face_done = Some(HashSet::new());
                            }
                            if name_from_face_done.as_mut().unwrap().
                                insert(facename.clone()) {
                                    Some(facename.clone())
                                } else {
                                    None
                                }
                        } else if name_from_face_done.is_some() {
                            debugln!(g," none");
                            None
                        } else {
                            let sn = v![g,e,source].name.clone();
                            debug!(g," n={}",sn);
                            Some(sn)
                        }
                    };

                    if let Some(namepart) = namepart {
                        debugln!(g," adding");
                        nameparts.push(namepart);
                    }
                }

                let nname = intern_uniquely_parts(
                    &mut intern,namesep,"",
                    nvs.len(), &nameparts,
                );

                let nvd = VertexData {
                    adj : Default::default(),
                    name : nname,
                    coordinates : None,
                    outer,
                };
                nvx.push(nv_edges);
                nvs.push(nvd);
            }
        }

        let nedges : Vec<_> = nedges
            .drain(..)
            .map(|ne| Rc::new(ne))
            .collect();

        for (vi, (ref mut nv, nv_edges)) in
            nvs.iter_mut().zip(&nvx).enumerate()
        {
            let mut found = HashSet::with_capacity(nv_edges.len());

            for &(nei, end) in nv_edges {
                debugln!(g,"vi={} nv=\"{}\"   nei={} end={}",
                          vi, nv.name, nei, end);
                let e = list::Pointer::with_selector(&nedges[nei], end);
                let e = EndRef(e);
                assert_eq!( e![ e,           .srcs ], vi );
                assert_ne!( e![ e.reverse(), .srcs ], INVX_INVALID );
                if !found.insert(e.target()) { Err(DualGivesMultipleEdge)?; }
                nv.adj.push_back(&e);
            }
        }

        Ok(PlanarGraph {
            vs : nvs,
            intern,
            debug : g.debug,
        })
    }
}

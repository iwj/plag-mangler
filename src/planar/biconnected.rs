// plag-mangler - planar graph handling utility and layout optimiser
//
// biconnected.rs - making graphs biconnected
//
//   Copyright (C) 2019 Ian Jackson
//
//  plag-mangler is free software: you can redistribute it and/or modify
//  it under the terms of the GNU Affero General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU Affero General Public License for more details.
//
//  You should have received a copy of the GNU Affero General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.



use super::*;

const DEBUG_FLAG_HERE : DebugFlags = DEBUG_AUGMENT;

// Our triangulation algorithm (see triangulate, below)
// requires a biconnected graph as input.
//
// Kant [1993] has an algorithm which seems very cumbersome.
// Here is my own:
//
// We will require that the input is connected and nontrivial.
//
// Lemma:
//
//  Suppose that C is a cut-vertex of an embedded planar graph G.
//  Then C occurs more than once in the cycle of vertexes around some
//  face F.
//
// Proof:
//
//  Let AA be a connected component of G' = G\{C}.
//
//  Let us draw a curve P starting at C and not otherwise touching G.
//  We will leave C in a direction (ie out of a corner) between an
//  edge from C to AA and an edge from C to some other component BB of
//  G'.  Now, we trace along just outside the outerface of AA.  The
//  outerface is a cycle like any face, so we will return to C.  The
//  inside of our curve contains at least AA and the outside at least
//  BB, and both of these have edges to C.
//
//  P does not cross G except at C, so it runs from C to C within a
//  face F of G.  The two ends of P at C are separated by at least
//  some edges of AA on the one side and some edges of BB on the
//  other, so these are two separate corners of F.  QED.
//
// Algorithm:
//
//  We detect this situation by walking each face F, counting the
//  number of times we encounter each vertex.  (That is, we walk the
//  face once to set the count to 0 for each vertex, and then walk it
//  again to process.)  This detects the dual occurrence of some C in
//  F.  Let A and B be the precdecessor and successor of an occurrence
//  of C in F.
//
//  A and B must be different: If they were the same, the two edges CA
//  and CB would be the same, and since they are adjacent at C, C has
//  only that one edge; that would make it not a cut-vertex, _|_.
//
//  AB cannot already be in G: there is a closed curve P from C
//  between A and B back to C, within F.  That divides space into two
//  regions, one containing A and one B.  The graph crosses P only at
//  C, but the edge AB would have to cross P.
//
//  We add the edge AB.  This splits a new face ACB from F, giving F'
//  as F with the corner ACB removed and replaced with the edge AB.
//
//  ACB is a triangle so has no double vertices.  F' has one fewer
//  occurrence of C and the same number of occurrences of all other
//  vertices.  So we repeat this procedure we will eliminate all
//  double vertices and hence all cut-vertices.
//
//  We need to make sure we will check F' for double vertices
//  (which might still include C).
//
//  We continue processing F' as if it were F, starting with
//  AB? (the new corner at B, next to the new edge AB).
//
//  If our starting corner in F was ACB, we treat ?AB (the new corner
//  at A) as the starting corner in F'.  That ensures we will have
//  visited every corner in F' once.
//
//  The encounter counts of A and B remain unchanged: the number of
//  times we have encountered A and B in F (up to and including ACB)
//  are the number if times we would have encountered A and B in F'
//  (up to and including ?AB).  The count for C is 1: in F' just
//  before ACB, we had enountered C once, since ACB is the 2nd
//  encounter, but we have obscured this encounter with the new edge
//  AB.
//
//  Observe that the stored encounter count is at most 1.
//
// Visiting faces:
//
//  We don't have a list of faces.  However, we can process each face
//  at least once by processing all edges.  We can avoid processing
//  the same face twice by marking each side of each edge as done, as
//  we go.
//
//  When we create the new edge BA above we may mark both sides of it
//  as done, and we may also mark the ACB sides of AC and CB.  (Since
//  both ACB and F' will be done when we have finished processing what
//  was originally F.)

// We abuse .invx as a flags word.
// We look at the face to the left of each edge.
const INVX_FACEDONE    : Invx = 0x0001;

fn process_face(g : &mut PlanarGraph,
                encountered : &mut Vec<bool>,
                mut s : EndRef) -> Result<()> {
    debugln!(g,"process_face {}", s.debug(g));
    for_face!{
        e, (g,s),
        debugln!(g,"process_face {} edge {} init", s.debug(g), e.debug(g));
        encountered[ e.source() ] = false;
    }
    for_face!{
        c, (g,s),
        debugln!(g,"process_face {} edge {} e={}", s.debug(g), c.debug(g),
                  encountered[ c.source() ] );
        if !encountered[ c.source() ] {
            encountered[ c.source() ] = true;
            continue;
        }

        let a = face_prev_edge(g,&c)?;
        let b = face_next_edge(g,&c)?;
        assert_ne!(a,b);
        let PerEnd([mut ab,_]) = add_edge_within(g, true, PerEnd([
            (&a, true),
            (&b, true),
        ]), &c)?;

        ab.m.borrow_mut().invx = PerEnd([ INVX_FACEDONE; 2 ]);
        // don't bother marking ACB as done; it's O(3) and fiddly

        if s == c { s = ab }
    }

    Ok(())
}

impl PlanarGraph {
    pub fn make_biconnected(&mut self) -> Result<()> {
        let g = self;
        g.set_all_invxs(0);
        let mut encountered = vec![ true; g.vs.len() ];
        for vi in 0..g.vs.len() {
            for mut e in &v!(g,vi).adj {
                {
                    let mut invx = em_mut![e,.invx];
                    if *invx & INVX_FACEDONE != 0 { continue }
                    *invx |= INVX_FACEDONE;
                }
                process_face(g, &mut encountered, EndRef(e))?;
            }
        }
        Ok(())
    }
}

// plag-mangler - planar graph handling utility and layout optimiser
//
// main.rs - command-line utility main program
//
//   Copyright (C) 2019 Ian Jackson
//
//  plag-mangler is free software: you can redistribute it and/or modify
//  it under the terms of the GNU Affero General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU Affero General Public License for more details.
//
//  You should have received a copy of the GNU Affero General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.

use std::io;
use std::fs::File;
use std::rc::Rc;

extern crate arrayvec;
extern crate libc;

// Rust complains that the macro_use isunused; but it is only unused
// in this module and it is used in other modules, but the macro_use
// most appear here or it does not work.
#[allow(unused_attributes)]
#[macro_use]
extern crate rc_dlist_deque;

mod planar;

use planar::PlanarGraph;

type Writer<'w> = &'w mut dyn io::Write;

fn graphviz_write(f : Writer,
                  g : &PlanarGraph,
                  fname : &str,
                  outside : &Option<Vec<usize>>,
                  pco : &Option<Vec<usize>>)
                  -> io::Result<()> {
    write!(f,"graph \"")?;
    planar::write_quoted(f, fname)?;
    writeln!(f,"\" {{")?;
    writeln!(f,"  splines=true;")?;
    g.graphviz_write(f,
                     Some(&mut |f,v|
    {
        if g.vi2outer(v) { write!(f," shape=box")?; }
        write!(f, " label=\"")?;
        planar::write_quoted(f, g.vi2name(v))?;
        write!(f,"\\n#{}",v)?;
        let somevec =
            |f:Writer, ary : &Option<Vec<usize>>, desc|
            -> io::Result<()>
        {
            if let Some(ary) = ary {
                if let Some(pos) = ary.iter().position(|e| *e == v) {
                    write!(f, "\\n{}#{}", desc, pos)?;
                }
            }
            Ok(())
        };
        somevec(f, pco,     "PCO")?;
        somevec(f, outside, "OS")?;
        write!(f, "\"")?;
        if let Some(ref c) = g.vi2coords(v) {
            const S : planar::Coordinate = 2.;
            write!(f," pos=\"{},{}\" pin=true", c[0] * S, c[1] * S)?;
        }
        Ok(())
    }),
                     Some(&mut |f,_vs,einfo|
    {
        let label = |f:Writer, what,ix| -> io::Result<()> {
            write!(f,"{}label=\"{}",
                   what, einfo.invx()[ix])?;
            if let Some(x) = einfo.facename(ix) {
                write!(f,"\\n")?;
                planar::write_quoted(f,&x)?;
            }
            write!(f,"\" ")?;
            Ok(())
        };
        label(f,"tail",0)?;
        label(f,"head",1)?;
        write!(f,"dir={}", match einfo.faceouter() {
            [false,false] => "none",
            [true, false] => "forward",
            [false,true ] => "back",
            [true, true]  => "both",
        })?;
        if einfo.added() { write!(f," style=dashed")?; }
        Ok(())
    }))?;
    writeln!(f,"}}")?;
    Ok(())
}

fn args_bufwriter<'s>(args : &'s mut dyn Iterator<Item=String>)
                      -> io::Result<(String, Box<dyn std::io::Write>)> {
    let fname = args.next().expect("argument for W/WG");
    let f : Box<io::Write> = {
        if fname == "-" { Box::new(io::stdout()) }
        else { Box::new(File::create(&fname)?) }
    };
    let f = io::BufWriter::new(f);
    Ok((fname, Box::new(f)))
}

fn main() -> std::result::Result<(),planar::Error> {
    let mut args = std::env::args().skip(1);
    let mut g = PlanarGraph::new();
    let mut pco = None;
    let mut outer = None;
    loop {
        let cmd = args.next();
        if cmd.is_none() { break }
        let cmd = cmd.unwrap();
        match cmd.as_str() {
            "D" => {
                let debug = planar::DebugFlags::from_str_radix(
                    &args.next().expect("argument for D"),
                    16).expect("hex debug level");
                g.set_debug(debug);
            },
            "R" => {
                let mut stdin = io::BufReader::new(io::stdin());
                g = g.file_read(&mut stdin)?;
            },
            "RF" => {
                let fname = args.next().expect("argument for RF");
                let mut f = io::BufReader::new(File::open(&fname)?);
                g = g.file_read(&mut f)?;
            },
            "W" => {
                let (_fname, mut f) = args_bufwriter(&mut args)?;
                g.file_write(&mut f)?;
            },
            "WG" => {
                let (fname, mut f) = args_bufwriter(&mut args)?;
                graphviz_write(&mut f, &g, &fname, &outer, &pco)?;
            },
            "W-FACES" => {
                let (_fname, mut f) = args_bufwriter(&mut args)?;
                g.faces_write(&mut f)?;
            },
            "PRINT-VI-NAMES" => {
                for vi in 0..g.nvertices() {
                    print!("#{} ", vi);
                    planar::write_quoted(&mut io::BufWriter::new(io::stdout()),
                                         g.vi2name(vi)).expect("stdout");
                    println!("");
                }
            },
            "SET-NAMES-VI" => {
                let nn : Vec<_> =
                    (0..g.nvertices())
                    .map(|n| Rc::new( n.to_string() ))
                    .collect();
                g.vs_set_names_all(&nn)?;
            },
            "CLONE" => { g = g.clone(); },
            "B" => { g.make_biconnected()? },
            "T" => { g.triangulate()? },
            "RAE" => { g.remove_added_edges() },
            "OUTER-V2F" => { g.outer_vertex2face()? },
            "OUTER-F2V" => { g.outer_face2vertex(false) },
            "OUTER-F2VA" => { g.outer_face2vertex(true) },
            "OUTER-SPLIT" => { g.split_outer('|')? },
            "OUTER-F12VA" => { // "one face to vertex array"
                outer = Some(g.find_outer()?);
            },
            "DUAL" => {
                g = g.dual('|')?;
            },
            "PCO" => {
                let outer = outer.as_ref().expect("wanted OUTER");
                if outer.len() != 3 { panic!("PCO outer not triangle"); }
                for &vi in outer {
                    if !g.vi2outer(vi) {
                        panic!("PCO outer bad vertex {}", g.vi2name(vi));
                    }
                }
                let mut oa = [0;3];
                oa.clone_from_slice(&outer);
                pco = Some(g.planar_canonical_order(oa)?);
            },
            "CP" => {
                g.chrobak_payne(pco.as_mut().unwrap());
            },
            "D-PCO" => {
                println!("pco={:?}", &pco);
            },
            "COORD-ADJ" => {
                let vi = args.next().expect("vertex num for COORD-ADJ")
                    .parse().expect("parse vertex num for COORD-ADJ");
                let mut accum = [0.;2];
                for a in &mut accum {
                    *a = args.next().expect("coordinate for COORD-ADJ")
                        .parse().expect("coordinae for COORD-ADJ");
                }
                let c = g.vi2coords(vi).unwrap_or([0.;2]);
                for (a,c) in accum.iter_mut().zip(c.iter()) {
                    *a += c;
                }
                g.vi_set_coords(vi, Some(accum));
            },
            "COORD-SET" => {
                let vi = args.next().expect("vertex num for COORD-ADJ")
                    .parse().expect("parse vertex num for COORD-ADJ");
                let mut newc = [0.;2];
                for a in &mut newc {
                    *a = args.next().expect("coordinate for COORD-SET")
                        .parse().expect("coordinae for COORD-SET");
                }
                g.vi_set_coords(vi, Some(newc));
            },
            #[cfg(feature="siman")]
            "ANNEAL" => {
                g.anneal()?;
            },
            "NLOPT" => {
                g.nlopt()?;
            },
            "OPTIM-COST" => {
                let cost = g.optim_cost()?;
                println!("cost={}", cost);
            },
            "OPTIM-NVIOLS" => {
                let viols = g.optim_viols()?;
                println!("nviols={}/{}",
                         viols.iter().filter(|&&v| v>0.).count(),
                         viols.len());
            },
            _ => panic!("unknown command"),
        }
    }
    Ok(())
}
